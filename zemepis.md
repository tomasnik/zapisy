# Tvar Země

## Obvod Země

 * vypočítán ve 3. století před našim letopočtu
 * How Eratosthenes calculated the Earth's circumference - Youtube
 * Geoid, zjednodušuje se na elipsoid
 * Referenční koule → poloměr 6371 km

## Zeměpisná šířka

 * Jde o úhel, který svírá rovina rovníku s rovinou příslušné rovnoběžky na povrchu Země.
 * Místa se stejnou zeměpisnou šířkou propojují rovnoběžky

## Zeměpisná délka

* Úhel, který svírá rovina nultého poledníku s místním poledníkem. (ten který prochází daným místem)

Na zkoušení: geoid, elipsoid referenční koule, poznat, kde je hranice geoidu/elipsoidu/..., souřadnice a ortodroma/loxxodroma.
# Postavení planety Země ve vesmíru

## Světelný rok

Světelný rok je jednotka vzdálenosti.
Je to vzdálenost, kterou světlo ve vakuu urazí za juliánský rok.

## Astronomická jednotka

- AU nebo UA
- vzdálenost Země od Slunce
1 AU = 149,6 milionů kilometrů

## Parsek

Parsek je jednotka vzdálenosti, kterou má 1 AU úhlový rozměr jedné vteřiny.

## Vznik vesmíru

Velký třesk → vznik vesmířu; před 13,8 miliardami let

Éra plazmatu → éra látky → zrychlená expanze

# Pohyby Země a jejich důsledky
 * Planeta Země je jednou z osmi planet Sluneční soustavy
 * Vzhledem k okolnímu vesmíru je v neustálém pohybu
## Pohyby Země
 * Rotace kolem zemské osy
 * Oběh kolem Slunce
 * Precesní pohyb zemské osy
 * Nutace zemské osy
### Rovina ekliptiky
 * Je rovina, ve které Země obíhá kolem Slunce. 
 * Téměř se nemění
### Rotace kolem osy
 * Zemská osa je myšlená přímka procházející jižním a severním pólemo * Země kolem této osy rotuje od západu na východ (proti směru hodinových ručiček)
 * Jedna otočka o 360 ° trvá 23 hodin, 56 minut a 4,09 sekundy. (siderický den)
 * Kvůli této rotaci se střídá den a noc
#### Siderický/sluneční den
Siderický: otočení vůči hvězdě, která je velmi daleko (oběh okolo Slunce je zanedbatelný) → 23h 56m

Sluneční: Země prakticky oběhne 361 °, protože o jeden stupeň se zhruba otočí Země kolem Slunce za jeden den (365 dní → 360 °) → 24 hodin
### Oběh Země kolem Slunce
**Geocentrismus**: vyvrácená teorie, že Země je střed vesmíru

**Heliocentrismus**: vyvrácená teorie, že Slunce je střed vesmíru

Ve skutečnosti je Slunce pouze středem Sluneční soustavy

**Afélium**: Místo, kde je Země nejdále od Slunce
 - v červenci

**Perihelium**: Místo, kde je Země nejblíže od Slunce
 - v lednu

**Rovnodenost**: jarní a podzimní, den i noc jsou všude stejně dlouhé (12 hodin)
**Letní slunovrat**: Den nejdelší, noc nejkratší (20.-21. 6.) dopadají paprsky kolmo na obratník Raka. Mezi severním polárním kruhem a severním pólem je polární den. Slunce zde nezapadá pod obzor. Na severní polokouli začíná léto.
## Precese
![Precese](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Praezession.svg/818px-Praezession.svg.png)
 * Vlivem nepravidelného rozložení hmoty na Zemi dochází gravitačním působením okolních těles (především Slunce a Měsíce) ke krouživému pohybu zemské osy (na obrázku P)
 * Zemská osa opisuje při tomto pohybu plášť dvojkužele s vrcholem ve středu Země
 * Jedna otočka trvá přibližně 27565 let
## Nutace
 * Přes precesní pohyb se překládá ještě jeden pohyb, označen N
 * Tento vlnivý pohyb zemské osy se jmenuje nutace
 * Hlavní příčinou nutace je periodicky se měnící postavení Měsíce a Slunce vůči Zemi
# Čas
## Sluneční čas
 * Lidé se v občanském životě řídí slunečním časem
 * čas určovaný otáčením Země vzhledem ke Slunci
 * Pravý sluneční den je doba mezi dvěma po sobě následujícími vrcholeními Slunce na místním poledníku
 * Jelikož se mění rychlost oběhu Země (v přísluní trvá den déle, než v odsluní) → zavádí se tzv. střední sluneční čas (nepravidelný pohyb Slunce byl nahrazen pravidelným pohybem)
### Střední sluneční čas
 * je čas měřený podle druhého středního slunce (myšleného bodu na nebeské sféře, který se pohybuje rovnoměrně po světovém rovníku východním směrem)
 * Jednotkou středního času je střední sluneční den - doba mezi dvěma průchody druhého středního slunce jedním poledníkem
![Image](https://upload.wikimedia.org/wikipedia/commons/4/49/Ob%C4%9B%C5%BEn%C3%A1_dr%C3%A1ha.jpg)
## Časová pásma
 * Každý poledník má svůj místní střední sluneční čas - je určen jako úhlová vzdálenost od druhého středního slunce
 * Tento úhel je převeden na čas (15° = 1 hodina, 15' = 1 minute, 15" = 1 sekunda)
 * Protože bylo nutné stanovit čas platný pro celá území, bylo třeba určit, který poledník bude pro dané území rozhodující
 * Světový čas (UTC) byl určen jako místní čas nultého poledníku
### UTC
 * UTC založen na atomových hodinách, tzn. je na rotaci Země nezávislý
* V Česku je používám středoevropský čas, který je místním sluečním časem patnáctého poledníku (15° východní délky), který prochází například Jindřichovým Hradcem.
# Pásmový x místní čas
## Místní čas
 * čas místního poledníku

24h/360=4 minuty → 1 ° jsou 4 minuty

Praha → 14,4 °

Brno → 16,6 °

Rozdíl je 2,2° → zhruba 8 minut

## Pásmový čas
 * jedno pásmo je široké 15 °
### Datová hranice
 * na východ se odečítá 24 hodin
 * na západ se přičítá 24 hodin
### Základní časová pásma
 * V ČR je SEČ (Středoevropský čas) → UTC+1
 * Případně SELČ, středoevropský letní čas → UTC+2
 * Ve skutečnosti je zimní čas o hodinu méně než standardní → prakticky se nepoužívá; standardní čas je chybně označován jako zimní
 * UTC - světový
 * Východoevropský → UTC+2
# Měsíc
## Velký impakt
 * teorie o vzniku Měsíce
 * do Země narazil velký objekt → oddělil se Měsíc
## Vázaná rotace
 * Měsíc se otočí za téměř stejnou dobu jako oběhne kolem Země, takže má proti Zemi otočenou stejnou stranu a odvrácenou druhou stranu
## Slapové jevy
 * příliv a odliv

Příliv
 - skočný (gravitační síly Slunce a Měsíce se sčítají → větší příliv)
 - hluchý (Slunce a Měsíc jsou v pravém úhlu → gravitační síly se téměř vynegují → menší příliv)
# Kartografická zobrazení
 * způsob znázornění zemského povrchu nebo jeho části (referenční plochy) do roviny mapy
 * způsob, který každému bodu na glóbu přiřazuje body v rovině
## Mapa
 * zmenšený a zjednodušený obraz povrchu
## Klasifikace zobrazení
 1. podle zobrazovací plochy
 2. podle polohy konstrukční osy
 3. podle vlastností z hlediska zkreslení
## Podle zobrazovací plochy
 * kuželová
 * válcová
 * azimutální
![Image](http://geologie.vsb.cz/praktikageologie/KAPITOLY/7_STER_PROJEKCE/PROJEKCE_JPEG/obr_7.1_p%C5%99%C3%ADklad_projekce.jpg)
 * nepravá
 * polykónická
 * víceplošná

Zobrazovacích ploch je více nebo jsou odvozeny matematicky bez použití zobrazových ploch
## Podle polohy konstrukční osy
 * normální
 * příčná
 * obecná
### Normální poloha
 * konstrukční osa je shodná se zemskou osou
### Příčná poloha
 * konstrukční osa leží v rovině rovníku
### Obecná poloha
 * konstrukční osa prochází středem glóbu v libovolném jiném směru
#### Křovákovo zobrazení

Křovákovo zobrazení je obecné konformní kuželové zobrazení, tj. zemský povrch je zobrazen na kuželu, který je v tzv. obecné poloze
### Podle vlastnosti z hlediska zkreslení
 * plochojevné - zachována plocha
 * úhlojevné - zachovány úhly
 * délkojevné - zachovány délky

Nejčastěji se používá vyrovnávací (kompenzační) - tak, aby zkreslení bylo v rovnováze
# Kartografie
 * vědní obor, který se zabývá tvorbou a zpracováním map, technikou jejich výroby a jejich využíváním
## Dělení map podle vzniku
 1. mapy původní (originální)
 2. mapy odvozené
## Fotogrammetrie
 * obor, který se zabývá rekonstrukcí tvaru, velikosti a polohy předmětů z leteckých snímků
## Etapy vzniku mapy
 1. astronomické práce
 2. geodetické práce
 3. topografické práce
 4. kartografické práce
 5. reprodukční práce
