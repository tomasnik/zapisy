# Živé organismy
Základní stavební a funkční jednotkou všech živých soustav je **buňka**.
Pro všechny živé systémy je typická **buněčná podstata** a **chemické složení**.
## Organické látky
 * bílkoviny
 * alkaloidy
 * sacharidy
 * tuky
 * nukleové kyseliny
 * barviva
 * hormony
## Anorganické látky
 * voda
 * železo
 * hořčík
 * vápník
## Hormony
 * rostlinné i živočišné
 * umožňují spouštění tělesných reakcí

Jsou dvě nukleové kyseliny, **DNA** a **RNA**. Jsou nositelem dědičnosti.

Barvivo je třeba **chlorofyl**, díky kterému může probíhat fotosyntéza.

**Silice** jsou v aromatických rostlinách.

**Pryskyřice** je ve stromech.

**Třísloviny** jsou látky nahořklé chuti a používají se v lékařství, léčitelství nebo jídlech.

**Voda** je velmi důležitá anorganická látka. Bez vody nastává dehydratace, která je velmi nebezpečná.

**Ionty** jsou důležité pro přenos kyslíku.

**Železo** je složkou hemoglobinu.

# Biogenní látky
 * prvky, z nichž se tvoří látky v organismu
 * **makrobiogenní:** C, H, O, N, P, S, Ca, Fe, K, Na, Mg, Cl do 50% sušiny
 * **mikrobiogenní:** Zn, Mn, Cu, I do 1 % sušiny
### ATP - adenozintrifosfát
 * tvořen dusíkovou bází, cukrem a třemi zbytky kyseliny fosforečné
 * zbytky jsou spojeny chemickou vazbou, ze kterých vychází energie
 * palivo pro buňku
 * organická látka
 * buňka si ji vyrábí sama v **mitochondrii**
 * nedokáže projít buněčnou membránou a buňka si ji tedy musí vyrábět pouze pro vlastní použití

Sušina je zbytek těla organismu po odstranění vody, obsahuje organické i anorganické látky.

Popelovina je zbytek těla organismu po spálení, obsahuje tedy pouze anorganické látky.
## Společné vlastnosti živých organismů
 * metabolismus
 * růst a vývin
 * rozmnožování (reprodukce)
 * dědičnost a variabilita
 * pohyb (i pohyb organel v buňce)
 * dráždivost
 * adaptace
 * vývoj
 * diferenciace (během evolučního procesu se od sebe mohou lišit)
## Viry
 * pouze se rozmnožují a liší se dědičná informace
 * rozmnožování jsou schopny pouze, když parazitují na jiném organismu
## Významné osobnosti v biologii
 * Aristoteles - 4. století př.n.l.
 * Ibn Sína - Avicenna - 1. tisíciletí n.l.
 * Jan Jesenský Jessenius - 16.-17. století
 * Antony von Leeuwenhoek - 17. století - zdokonalil mikroskop
 * Robert Cook - 17. století - poprvé popsána rostlinná buňka
 * Carl von Linné - 18. století - systém zařazování organismů
 * Charles Darwin - 19. století - evoluční teorie
 * Johan Gregor Mendel - 19. století - genetika, v Brně
 * Jan Evangelista Purkyně - 19. století - fyziolog, podílel se na tvorbě buněčné teorie
 * T. Schwann a M. J. Schleiden - taky buněčná teorie
 * Louis Paster - 19. století - mikrobiolog, který vyvrátil teorii samoplození, autor očkování proti vzteklině a některým onemocněním hospodářských zvı́řat
 * Francis Crick, James Watson, Maurice Wilkins, Rosalind Franklin - 20. století

Crick a Watson dostali Nobelovu cenu za objev nukleových kyselin, Franklin studovala rentgenové záření a na následky zemřela
## Třídění biologických věd

1. Podle druhu zkoumaného organismu
	* zoologie
	* botanika
	* anthropologie
	* mikrobiologie
	* mykologie
2. Vědy, které studují vlastnosti organismů
	* anatomie
	* morfologie
	* cytologie - buňky
	* histologie - stavba tkání 
	* organologie
	* fyziologie
	* genetika
	* ekologie
	* etologie - o chování organismů
3. Vědy, které studují živé soustavy z obecného hlediska
	* vývojová geologie - ontogeneze → vývoj jedince od jeho vzniku až do smrti
	* evoluční biologie
4. Hraniční obory
	* biochemie
	* biofyzika
	* biomatematika
	* biogeografie
5. Aplikované obory
	* lékařství
	* genové inženýství
	* biotechnologie

Vědecké metody, které používají biologické vědy:

1. Pozorování
2. Experiment
3. Modelování

## Rozdělení organismů

Vědní obor, který se zabývá tříděním organismů do systematických jednotek = __taxonů__, se nazývá __taxonomie__ = systematika (klasifikace).

Do taxonů se řadí organismy podle různých hledisek - tvar těla, vlastnosti, ekologie atd.

* Taxony jsou hierarchicky uspořádány
* Základní taxon je druh. Každý druh má latinský název, za název druhu se píše jméno autora, který druh popsal poprvé → L. - Linné. Všechny organismy nemají český název.
* Základní taxony: říše, kmen, třída, řád, čeleď, rod, druh

Doména: Prokaryota

Říše: Bakterie

Říše : Archea

---

Doména: Eukaryota

Říše: Jednobuněční (prvoci)

Říše: Chromista

Říše: Rostliny

Říše: Houby

Říše: Živočichové

# Prokaryota (prvojaderní)

 * Prokaryotická buňka o velikosti 1-10 mikrometrů (pozorovatelné mikroskopem)

Cytoplazmatická membrána

 * Každá buňka je ohraničena cytoplazmatickou membránou
 * Cytoplazmatická membrána se skládá ze dvou vrstev fosfolipidů, které jsou prostoupeny bílkovinami
 * Cytoplazmatická membrána je polopropustná (semipermeabilní)

Buněčná stěna

 * Buněčná stěna je propustná (permeabilní)
 * Buňky u rostlin a hub mají buněčnou stěnu

Slizové pouzdro

 * Slizové pouzdro brání buňku proti nepříznivým podmínkám
 * Může se změnit ve spóru (cystu) (zbaví se vody, aby nepraskla)

Bičík

 * Jeden nebo více
 * Většinou u buněk s tyčkovitým tvarem

Další

 * Vlákna (glykokalyx) - polysacharid; umozňuji, aby se buňka lépe uchytila 
 * Fimbrie - bílkovinná vlákna, propojují se přes něj buňky → výměna genetické informace
 * Mesozom (vychlípenina cytoplazmatické membrány), často u aerobních buněk (ke svému životu potřebuji vzduch/kyslík), má funkci mitochondrií
 * Buňka je vyplněna cytoplazmou
 * Genetická informace je uložena v cyklické molekule DNA (nukleoid/bakteriální chromozom)
 * DNA (DNK) - Deoxyribonukleová kyselina
 * Plazmid - malá cyklická molekula DNA, obsahuje geny 
 * Ribozomy - určuje, jak se mají chovat bílkoviny
 * Granula - odpadní nebo zásobní látky, mohou být tekuté nebo pevné
 * Tylakoidy - tělíska, která umožňují fotosyntézu, obsahují fotosyntetická barviva (clorofyl)
 * Plynové vakuoly - nadnáší buňky

Během fotosyntézy vzniká kyslík a glukóza.
Bílkoviny můžou za spoustu věcí, DNA říká, jak se mají bílkoviny chovat.

# Archea

 * Jiné chemické složení než bakterie
 * Metanové archebakterie - vyskytují se v bahně a v zažívací soustavě živočichů, vyrábí bioplyn, nepotřebují k životu kyslík
 * Slanomilné archebakterie - vyžadují kyslík, jsou v slaných jezerech, mořích, nasoleném masu,… 
 * Hypertermofilní archebakterie - aerobní i anaerobní, v horkých pramenec (až 100 °C), sirné horké prameny

# Bakterie

Dělení podle tvaru buňky:

 * kulaté - koky
 * dvojice koků - diplokoky
 * řetízky - streptokoky
 * hrozny - stafylokoky
 * tyčkovité - tyčky
 * opouzdřené tyčky - bacily
 * zahnuté tyčky - vibria
 * prohnuté (vícekrát) tyčky - spirily
 * spirálně stočené tyčky - spirochety
 * bičíkaté bakterie (tyčky)
 * mykobakterie - rozvětvené podhoubí
 * aktinomycety - větvené (připomínají stromeček)

# Výživa prokaryot

## Podle zdroje uhlíku:

 * autotrofní - CO2
 * heterotrofní - jiná organická látka

## Podle zdroje energie (jakou energii dokáží využít)

 * fototrofní - zdroj energie je sluneční záření
 * chemotrofní - zdroj energie jsou organické nebo anorganické látky, které si organismy oxidují

## Podle vztahu ke kyslíku:

 * aerobní organismy - kyslík je pro ně nepostradatelný, kyslík je konečný příjemce elektronů, které se odeberou při dýchání
 * striktní (obligátní) aeroby - organismy, které se neobejdou bez kyslíku
 * anaerobmí organismy - kyslík nepotřebují, konečný příjemce elektronů je například dusičnan
 * striktní (obligátní) anaeroby - hynou v přítomnosti kyslíku
 * fakulativně anaerobní organismy - organismy upřednostňují kyslíkaté prostřeí, když není kyslík, mají anaerobní metabolismus.


 * G+ → Grampozitivní bakterie → silná buněčná stěna
 * G- →  Gramnegativní bakterie → tenká buněčná stěna
Podle Gramova barvení, G+ fialová, G- byla červená

# Rozmnožování bakterií

 * zdvojení nukleoidu
 * příčné dělení - rychlé a energeticky nenáročné

# Bakterie dle prostředí výskytu

## Bakterie žijící v půdě a ve vodě
 * Největší množství bakterií
### Saprofytické bakterie 
 * rozklad organických látek a jejich přeměna na látky anorganické - mineralizace
 * Další funkce: koloběh látek, zúrodňování půdy, samočištění vod
 * Některé patogenní
### Hlízkové bakterie
 * fixace dusíku
 * symbióza s bobovitými rostlinami
 * r. Rhizobium
### Nitrifikační bakterie
 * oxidace amonných solí a dusitanů na dusičnany
 * r. Nitrobacter, Nitrosococcus
### Denitrifikační bakterie
 * ochuzování půdy o dusík
 * r. Pseudomonas
### Bakterie produkující metan
### Sirné bakterie

## Bakterie žijící ve vzduchu
 * Saprofytické nebo parazitické druhy
 * Do vzduchu - druhotně - vířeným prachem, v kapénkách slin, hlenu, potu
### Bakteriální onemocnění
#### Bakteriální nákaza (infekce)
 * vstup a pomnožení bakterií v organismu člověka, zvířete nebo rostliny
#### Patogenní bakterie 
 * bakterie, které jsou schopny vniknout do těla organismu a vyvolat v něm onemocnění
#### Šíření (přenos) nákazy:
 * **Přímý přenos** - stykem mezi zdrojem a novým hostitelem (zvířecí kousnutí, polibek, pohlavní styk)
 * **Nepřímý přenos** - kapénkami, půdou, vodou, potravinami, znečištěnými předměty,...
2018-09-21
Inkubační doba - doba, která je zaotřebí k tomu, aby se bakterie po vniknutí do organismu pomnožila a vyvolala první příznaky onemocnění
 * Epidemie - hromadné rozšíření nakažlivé nemoci
 * Pandemie - rozšíření nakažlivé nemoci ve velkém rozsahu (světadíl)
 * Endemie - onemocnění vyskytující se jen v určité oblasti
#### Bakteriální onemocnění člověka
##### Nákazy přenášené vzduchem
###### Tuberkulóza (TBC) - (Kochův bacil)
 * mykobakterie
 * většinou plicní onemocnění
 * inkubační doba 6-8 týdnl
 * zdroj nákazy - člověk, krávy
 * očkování
###### Záškrt
 * Korynebakterie
 * Projevy jako silná angína, poškození srdce, může způsobit smrt
 * Inkubační doba 2-5 dní
 * Děti
 * Zdroj nákazy - člověk
 * očkování
###### Dávivý kašel (černý kašel)
 * Kulovitá bakterie 
 * Děti
 * Inkubační doba 6-8 dní
 * Zvýšená teplota, kašel, dušnost, zvracení
 * očkování
###### Angína
 * Streptokoky
 * Postihuje mandle a hltan
 * Zvýšená teplota, bolest krku a hlavy,...
 * Inkubační doba 1-3 dny
###### Spála
 * streptokoky
 * horečky, zvracení, bolest v krku, bolest hlavy, vyrážka, odlupování kůže
###### Růže
 * streptokoky
 * horečka, zardunutí a zduření určitého místa kůže
 * inkubační doba 2-5 dní
##### Nákazy, jejichž vstupní branou jsou ústa
###### Břišní tyf
 * Salmonela tyfu
 * Střevní onemocnění
 * Inkubační doba 14-16 dní
 * Silná bolest hlavy, únava, horečka, průjmy, krvácení ze střev
 * Očkování (rozvojové země V a J Asie, Mexiko, J. Amerika, Afrika)
###### Salmonelóza
 * Inkubační doba 12-24 hodin
 * Nevolnost, mrazení, malátnost, bolesti hlavy, svalů a kloubů, křeče v břiše, zvracení, průjmy, horečky
###### Úplavice
 * Šigela
 * Střevní onemocnění
 * Inkubační doba 2-3 dny
 * Bolesti břicha, horečka, průjmy
###### Cholera
 * Vibrio
 * Inkubační doba 2-3 dny
 * Nevolnost, zvracení, průjmy - odvodňování tkání - poruchy krevního oběhu, teplota 35,8 °C, při neléčení smrt z vyčerpanosti
 * Očkování v rozvojových zemích
###### Stafylokoková nákaza
 * Stafylokok → enterotoxin
 * Otrava s bolestmi hlavy, křečemi v břiše, nevolností, zvracením, průjmy
 * Onemocnění přejde během 2 až 3 dnů
###### Botulismus
 * Clostridium botulinum
 * Neurotoxin botulin (klobásový jed)
 * Zdroj nákazy - infikovaná potrava
 * Působí na nervové buňky
 * Dvojité vidění, porucha polykání, obrny dechových svalů
##### Nákazy, jejichž vstupní branou je poraněná kůže
###### Tetanus
 * Bacil (Clostridium tetani)
 * Inkubační doba 7-14 dnů
 * Stahy svalstva žvýkacího, šíjového, později i chrupu, nastává smrt udušením
 * Při poranění znečištěném půdou nebo hnojem
 * Očkování
###### Stafylokokové kožní nákazy
 * Stafylokoky
 * Hnisavé záněty kůže, vředy,…
###### Trachom
 * Chlamydie
 * Inkubační doba 5-12 dnů
 * Onemocnění zevního oka
 * Začíná zánětem spojivek a končí zjizvením rohovky → ztrátou zraku
##### Onemocnění přenášené pohlavním stykem
###### Syfilis (příjice)
 * Treponema
 * Probíhá ve třech stádiích
 1. Tvrdý vřed
 2. Vyrážky
 3. Napadeny velké cévy, centrální nervový systém, játra, kosti, kůže - degenerace mozku a míchy
 * Inkubační doba 3 dny (10 dní až 10 týdnů)
###### Kapavka (gonorrhoea)
 * Malé diplokoky
 * Prudký zánět sliznice pohlavních orgánů
##### Nákazy přenášené zvířaty
###### Sněť slezinná (antrax)
 * Bacil sněti slezinné (Bacillus anthracis)

Kožní forma: na kůži červená skvrna - puchýřek - praskne - vřed se zčernalým středem, inkubační doba 2-3 dny

Plicní forma: inkubační doba 1 den, téměř 100%  úmrtnost

Střevní forma: požití tepelně nezpracovaného masa nemocného zvířete, krvavé průjmy, bolesti
###### Borelióza
 * spirálovitá bakterie (spirochetou) (Borrelia burgodorferi)
 * Přenos: klíště obecné a jiné druhy sajícího hmyzu

2 stádia:

 1. Časně: červená skvrna s bílým místem uprostřed, inkubační doba 7-14 dní, horečka, bolesti svalů, únava, později různé projevy postižení nervového, kardiovaskulárního a kloubního systému
 2. Pozdní: za více než 6-12 měsíců - postižení kloubů, kůže, nervového systému, má chronický průběh
 * Očkování jen v USA - vakcína Lymerix
###### Mor
 * tyčkovitá bakterie 
 * přenos: blecha morová (z hlodavců)
 * různé formy morové infekce - dýmějový mor
 * záněty mízních uzlin, při prasknutí celková sepse organismu
 * dnes výskyt J. a S. Amerika, Asie, J. Afrika, různé formy morové infekce
##### Symbiotické bakterie v těle člověka
###### Lactobacillus
 * trávicí, pohlavní soustava
 * produkuje kyselinu mléčnou a vytváří kyselé prostředí
###### Escherichia coli
 * střevní bakterie
##### Bakteriální nákazy rostlin
 * do rostliny přes poranění nebo průduchy

Choroby:

 * měkká hniloba - mrkev, brambory, zelí, celer
 * vadnutí - rajčata, kukuřice, zelí
 * skvrnitost - rajčata
 * rakovina (nádory) - ovocné stromy
##### Zneškodňování bakterií
 1. Sterilizace - odstranění všech živých organismů (i bakterií) z předmětů 
 - zahřívání na 80 °C - 120 °C, var, vysoký tlak (autokláv)
 2. Dezinfekce - usmrcení škodlivých mikroorganismů (i bakterií)
 - dezinfekční látky (chemické látky) - alkohol (70% etanol, jód, mýdla)
 3. UV záření - ničí všechny bakterie
## Význam bakterií
 * patří k nejstarším organismům
 * nejrozšířenější skupina živých organismů
 * reducenti (dekompozitoři) - rozklad odumřelé organické hmoty (mineralizace)
 * význam pro koloběh látek, jako symbiotické organismy, jako výrobní prostředek v biotechnologiích
 * hydrolytické bakterie - hydrolýza org. látek
 * původci bakteriózy
 * výroba antibiotik, octa, mléčných výrobků
### Bakterie využívané v průmyslové výrobě
 * výroba octa (Acetobacter aceti)
 * máselné kvašení (Clostridium butyricum)
 * antibiotika (streptomyces)
 * mléčné výrobky (streptoccocus)
 * jogurty (Lactobacillus bulgaricus)
# Sinice 
 * autotrofní organismy
 * buňka sinic
 * typická prokaryotní buňka s thylakoidy - obsahující chlorofyl a, karetonoidy a fykobiliny (fykocyanin a fykoerythrin)
 * plynové vakuoly
 * pohyb pomocí slizu
 * jednobuněčné sinice - spojené slizovými obaly
 * vláknité sinice
## Rozmnožování sinic
 * pomocí akinet
 * pomocí hormogonie
## Akinety
 * buňka sinic
 * forma sinic na přežití v nepříznivých podmínkách
 * můžou se takhle rozmnožovat
## Výskyt sinic
 * na ledovcích
 * na pouštích
 * v termálních pramenech
 * ve sladkých i slaných vodách
## Eutrofizace vod
 * obohacování vod o živiny (dusík a fosfor)
 * kvůli septikům a splachům z polí
 * důsledkem je vznik vodního květu → přemnožení sinic

travertin

# Viry
 * subcellulata
 * 15-300 nm
 * pozorovatelné elektronovým mikroskopem
 * jeden z prvních byl virus tabákové mozaiky
 * nejčastěji kulovitý nebo tyčinkovitý a pak příbuzné tvary
## Virion
 * částice viru, která je schopna napadnout buňku
### Stavba virionu
 * **molekula nukleové kyseliny**
 * **kapsid** - bílkovinný obal, skládá se z bílkovin kapsomer, udává tvar virionu 
 * **(obal)** - vir může být obalen lipidovou membránou
 * **(enzymy)** - zahajují syntézy v hostitelské buňce; **reverzní transkriptáza** → změna z RNA na DNA
## Reprodukce
 * adsorpce
 * penetrace
 * změna metabolismu
 * lytický nebo lyzogenní cyklus
### Lytický cyklus
 * v napadených buňkách začnou během krátké doby vznikat viriony
 * po nějaké době praskne → lyze buňky → uvolní se další viriony
### Lyzogenní cyklus
 * fágová DNA se začlení do napadené buňky
 * při replikaci se pak replikuje i fágová DNA
 * **latentní fáze** → při oslabení se může vyvolat přechod do lytického cyklu
### Rezistentní buňka
 * virion nepřilne (buňka nemá vazebná místa)
 * genetická informace virionu se nemůže v buňce projevit
### Infekce
 * vstupují do organismu přes sliznice, dýchací cesty, trávicí soustava, průduchy
 * šíření infekce - tělními tekutinami, nervovými vlákny, cévními svazky
 * viróza - poškození organismu a vyvolání nemoci
 - akutní
 - chronická
