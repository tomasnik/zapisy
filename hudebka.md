# Hudba pravěku

## Nálezy

 * Paleolit - starší doba kamenná
 - byly nalezeny opracované kameny (hudební nástroj - činely)
 - rohy zvířat - používány jako dechové hudební nástroje
 - buď pro zábavu nebo svolávání
 * Neolit - mladší doba kamenná
 - píšťaly/rohy zvířat s dírkami → jiný tón
 - bubny - opracovaná kůže zvířat na třeba vydlabaném pařezu
 - používali ruce, klacky i kameny
 * Doba bronzová
 - činely
 - kovové rohy
 - výroba drnkacích nástrojů
 - předchůdce harf 
 - struny nejdříve ze střev zvířat (nebyla zvučná), poté kovová struna (zvučnější)
 - zvony - velmi důležité → určování času, nebezpečí, při slavnostech
# Hudba starověku

## Kultura egyptská (3500 př.n.l.)
 * nevíme, jak zapisovali noty
 * máme obrazy hudebních nástrojů na školách, byli nalezeni hráči na lyry, loutny
## Kultura čínská
 * pentatonika
## Kultura židovská
 * zdokonalování hudebních nástrojů
 * *žalmy* jsou **židovské náboženské zpěvní texty**
## Kultura řecká
 * položila základy evropské hudby
 * hudební základy souvisí s vývojem divadla a dramatu
 * původně bylo v dramatech hodně zpěvu
 * neví se, jak zapisovali noty
 * slovo muzika pochází z řečtiny
 * Pythagoras položil základy hudební teorie - založil oktávu, stupnici
 * nejstarší celou hudební pamatkou je Seikilova píseň
# Duchovní hudba středověku
## Gregoriánský chorál
604 n.l. papež Řehoř Veliký (Gregor)

 * založil hudební školu
 * rozhodl se dát všechny duchovní písně do sborníku
 * 14 let sbíral všechny dostupné duchovní písně a dal je do sborníku pojmenovaného *Gregoriánský chorál*
 * zpívá se "a capella" → bez doprovodu
 * pouze latinsky a text byl většinou o Ježíši a Marii
## Nejstarší česká duchovní píseň
**Hospodine, pomiluj ny**

 * bez doprovodu hudebních nástrojů
 * staročeský text
 * vznikla v 10. století
 * zmiňuje se o ní Kosmas
 * poprvé zpívána v roce 1055 při korunovaci Spytihněva II. ale vznikla dříve
 * první záznam je v kronice břevnovského benedyktýna Jana z Holešova roku 1397
 * měla funkci hymny → proto zpívána při slavnostních událostech
## Druhá nejstarší česká duchovní píseň
**Svatý Václave!**

 * chorál
 * vznikla ve 12. století, první zápisy textu pocházejí ze 14. století a nápěvu z 15. století
 * v dnešní podobě používána od roku 1668
 * také měla funkci hymny
